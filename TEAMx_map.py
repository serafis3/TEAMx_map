#!/usr/bin/env python

import numpy as np
from matplotlib import pyplot as p
import cartopy.crs as ccrs
import matplotlib
matplotlib.use('agg') # Agg is a non-interactive renderer; module won't fail when trying to create a figure after disconnecting from an x-server

def read_csv(input_filename):
    import csv

    # Initialize empty lists
    site = []
    lat = []
    lon = []
    elev = []
    obs_type = []

    # Parse csv file and read info about station name/lat/lon
    with open(input_filename, 'rt', encoding='latin_1') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        headers = next(reader)#[1:]
        if len(headers)==5:
            for row in reader:
                site.append(row[0])
                lat.append(row[1])
                lon.append(row[2])
                elev.append(row[3])
                obs_type.append(row[4])
        elif len(headers)==4:
            for row in reader:
                site.append(row[0])
                lat.append(row[1])
                lon.append(row[2])
                elev.append("")
                obs_type.append(row[3])
        elif len(headers)==3:
            for row in reader:
                site.append(row[0])
                lat.append(row[1])
                lon.append(row[2])
                elev.append("")
                obs_type.append("")
    return site,lat,lon,elev,obs_type

def write_kml(input_filename,output_filename_kml):
    import csv
    import simplekml
    
    # Paths to GE icons
    GEnames = {'grnPushpin': 'pushpin/grn-pushpin.png',
           'redPushpin': 'pushpin/red-pushpin.png',
           'whtPushpin': 'pushpin/wht-pushpin.png',
           'ltbluPushpin': 'pushpin/ltblu-pushpin.png',
           'bluePushpin': 'pushpin/blue-pushpin.png',
           'ylwPushpin': 'pushpin/ylw-pushpin.png',
           'purplePushpin': 'pushpin/purple-pushpin.png',
           'pinkPushpin': 'pushpin/pink-pushpin.png',
           'redCircle' : 'shapes/placemark_circle_highlight.png' }

    # Parse csv file and encode data in KML for GoogleEarth display
    with open(input_filename, 'rt', encoding='latin_1') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        kml = simplekml.Kml()
        kml.document = simplekml.Folder(name = "TEAMx observational resources")
        headers = next(reader)[1:]
        if len(headers)==4:
            # Define folders
            ibox = kml.newfolder(name="i-Box")
            fluxnet = kml.newfolder(name="FluxNet")
            other = kml.newfolder(name="Other")
            tawes = kml.newfolder(name="ZAMG TAWES")
            hybz = kml.newfolder(name="Hydrographisches Amt Südtirol")
            mtn = kml.newfolder(name="MeteoTrentino")
            for row in reader:
                # Shorten label names (info already given in folder name)
                if 'i-Box' in row[0]:
                    pnt = ibox.newpoint(name=row[0].replace('i-Box ',''), coords=[(row[2],row[1])])
                    displaystyle = 0
                elif 'FluxNet' in row[0]:
                    pnt = fluxnet.newpoint(name=row[0].replace('FluxNet ',''), coords=[(row[2],row[1])])
                    displaystyle = 0
                elif 'OTHER' in row[0]:
                    pnt = other.newpoint(name=row[0].replace('OTHER ',''), coords=[(row[2],row[1])])
                    displaystyle = 0
                elif 'TAWES' in row[0]:
                    pnt = tawes.newpoint(name=row[0].replace(' TAWES',''), coords=[(row[2],row[1])])
                    displaystyle = 1
                elif 'HYBZ' in row[0]:
                    pnt = hybz.newpoint(name=row[0].replace('HYBZ ',''), coords=[(row[2],row[1])])
                    displaystyle = 1
                elif 'MTN T0' in row[0]:
                    pnt = mtn.newpoint(name=row[0].replace('MTN ',''), coords=[(row[2],row[1])])
                    displaystyle = 1
                # Define different display obs_type
                # Currently 0 and 1 are for "visible" and "invisible" points, respectively
                if displaystyle == 0:
                    pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/%s'%GEnames[row[4]]
                elif displaystyle == 1:
                    pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/%s'%GEnames[row[4]]
                    # invisible at startup
                    pnt.visibility=0
                    # label name only appears on mouse hover
                    pnt.stylemap.normalstyle.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/%s'%GEnames[row[4]]
                    pnt.stylemap.normalstyle.labelstyle.scale = 0
                    pnt.stylemap.highlightstyle.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/%s'%GEnames[row[4]]
                    pnt.stylemap.highlightstyle.labelstyle.scale = 1
        elif len(headers)==2:
            for row in reader:
                pnt = kml.newpoint(name=row[0], coords=[(row[2],row[1])])
        kml.save(output_filename_kml)

def set_plotting_style(obs_type):

    # Based on style, set
    # markers, sizes, edgecolors, facecolors, labels
    markers = []
    sizes = []
    edgecolors = []
    facecolors = []
    labels = []
    linewidths = []
    for i in range(len(obs_type)):
        if obs_type[i] == "AWS":
            markers.append('o')
            sizes.append(2)
            linewidths.append(0)
            edgecolors.append('None')
            facecolors.append('k')
            labels.append('Automatic weather station')
        elif obs_type[i] == "EC":
            markers.append('o')
            sizes.append(10)
            linewidths.append(0.75)
            edgecolors.append('k')
            facecolors.append('r')
            labels.append('Eddy covariance site')
        #
        elif obs_type[i] == "RaSo":
            markers.append('P')
            sizes.append(30)
            linewidths.append(0.75)
            edgecolors.append('k')
            facecolors.append('w')
            labels.append('Radiosonde')
        elif obs_type[i] == "Drones":
            markers.append('P')
            sizes.append(30)
            linewidths.append(0.75)
            edgecolors.append('k')
            facecolors.append('gold')
            labels.append('RPAS')
        #
        elif obs_type[i] == "CbandRadar":
            markers.append('*')
            sizes.append(30)
            linewidths.append(0.75)
            edgecolors.append('k')
            facecolors.append('gold')
            labels.append('C-band radar')
        elif obs_type[i] == "XbandRadar":
            markers.append('*')
            sizes.append(30)
            linewidths.append(0.75)
            edgecolors.append('k')
            facecolors.append('orange')
            labels.append('X-band radar')
        elif obs_type[i] == "RainRadar":
            markers.append('*')
            sizes.append(30)
            linewidths.append(0.75)
            edgecolors.append('k')
            facecolors.append('green')
            labels.append('Rain radar')
        #
        elif obs_type[i] == "WindProfiler":
            markers.append('^')
            sizes.append(15)
            linewidths.append(0.75)
            edgecolors.append('k')
            facecolors.append('r')
            labels.append('Radar wind profiler')
        elif obs_type[i] == "TRHProfiler":
            markers.append('^')
            sizes.append(15)
            linewidths.append(0.75)
            edgecolors.append('k')
            facecolors.append('b')
            labels.append('Microwave radiometer')
        #
        elif obs_type[i] == "DWL":
            markers.append('s')
            sizes.append(15)
            linewidths.append(0.75)
            edgecolors.append('k')
            facecolors.append('g')
            labels.append('Doppler wind lidar')
        elif obs_type[i] == "AerosolProfiler":
            markers.append('s')
            sizes.append(15)
            linewidths.append(0.75)
            edgecolors.append('k')
            facecolors.append('w')
            labels.append('Ceilometer')
        elif obs_type[i] == "RamanLidar":
            markers.append('s')
            sizes.append(15)
            linewidths.append(0.75)
            edgecolors.append('k')
            facecolors.append('b')
            labels.append('Raman lidar')
        elif obs_type[i] == "WVDIAL":
            markers.append('s')
            sizes.append(15)
            linewidths.append(0.75)
            edgecolors.append('k')
            facecolors.append('gold')
            labels.append('Water vapor DIAL')
        elif obs_type[i] == "AtmosChem":
            markers.append('o')
            sizes.append(0)
            linewidths.append(0)
            edgecolors.append('None')
            facecolors.append('None')
            labels.append(None)
        elif obs_type[i] == "AQ":
            markers.append('o')
            sizes.append(0)
            linewidths.append(0)
            edgecolors.append('None')
            facecolors.append('None')
            labels.append(None)
        else:
            markers.append('o')
            sizes.append(0)
            linewidths.append(0)
            edgecolors.append('None')
            facecolors.append('None')
            labels.append(None)

    return markers,sizes,edgecolors,facecolors,labels,linewidths

def draw_map_background(ax,mapextent,zoomlevel = 9,xgrid=np.arange(-180,180,5),ygrid=np.arange(-90,90,5),scale="10m",inland_water_bodies_on=False,admin_boundaries_on=False,lw=0.75,linecolor='black',zorder_grid=10):
    import matplotlib.ticker as mticker
    import cartopy.io.img_tiles as cimgt
    from cartopy.feature import NaturalEarthFeature
    from cartopy.mpl.gridliner import LATITUDE_FORMATTER, LONGITUDE_FORMATTER

    # Set map projection & domain sizes
    ax.set_extent(mapextent,crs=ax.projection)

    # Draw map background
    # Create a terrain background instance.
    # https://scitools.org.uk/cartopy/docs/latest/gallery/scalar_data/eyja_volcano.html
    # then add the data at the desired zoom level.
    # terrain = cimgt.Stamen('terrain')
    # terrain = cimgt.OSM()
    terrain = cimgt.GoogleTiles(style='satellite')
    ax.add_image(terrain, zoomlevel, alpha=0.5)

    # Add vector data
    ax.coastlines(resolution=scale, color=linecolor, linewidth=lw)
    if admin_boundaries_on:
        regions_1 = NaturalEarthFeature(category='cultural', scale=scale, facecolor='none',name='admin_1_states_provinces_shp')
        ax.add_feature(regions_1, edgecolor=linecolor,linewidth=lw)
    if inland_water_bodies_on:
        rivers_1 = NaturalEarthFeature(category='physical', scale=scale, facecolor='none',name='rivers_lake_centerlines')        
        ax.add_feature(rivers_1, edgecolor='#30A0D0',linewidth=lw)
        lakes_1 = NaturalEarthFeature(category='physical', scale=scale, facecolor='none',name='lakes')        
        ax.add_feature(lakes_1, edgecolor='#30A0D0',facecolor='#30A0D0',linewidth=lw)
    state_boundaries = NaturalEarthFeature(category='cultural', scale=scale, facecolor='none',name='admin_0_boundary_lines_land')        
    ax.add_feature(state_boundaries, edgecolor=linecolor,linewidth=lw*2)
    ax.add_feature(state_boundaries, edgecolor='white',linewidth=lw,linestyle='--')

    # Add map grid
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, x_inline=False, y_inline=False, rotate_labels=False,
                  linewidth=1, color='w', alpha=1, linestyle='--',zorder=zorder_grid)
    gl.top_labels = False
    gl.right_labels = False
    gl.bottom_labels = True
    gl.left_labels = True
    gl.xlocator = mticker.FixedLocator(xgrid)
    gl.ylocator = mticker.FixedLocator(ygrid)
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER

    return ax

def draw_toc_info(ax,title,sites,lons,lats,obs_types,add_domains=False) -> p.Axes:
    """_Adds pins to map_

    Args:
        ax (_type_): _description_
        title (_str_): _description_
        sites (_list_): _description_
        lons (_list_): _description_
        lats (_list_): _description_
        obs_types (_list_): _description_
        add_domains (bool, optional): _description_. Defaults to False.

    Returns:
        ax: _description_
    """
    # Sanity check
    assert len(sites) == len(lons) == len(lats) == len(obs_types)

    # Convert lists into numpy arrays
    alons = np.array(lons).astype(float)
    alats = np.array(lats).astype(float)
    sites = np.array(sites)
    unique_sites = np.unique(np.array(sites))

    # Tweak longitudes of non-unique sites, to avoid overlapping map pins
    # At sites with multiple instruments, pins are aligned horizontally
    for i in unique_sites:
        duplicate_indices = np.where(sites == i)[0]
        s = duplicate_indices.size
        if s > 1:
            #alons[duplicate_indices] += np.arange(duplicate_indices.size)*0.04
            alons[duplicate_indices] += ((-1)**np.arange(s))*0.02
            alats[duplicate_indices] += ((np.arange(s)//2*2)//2)*0.025

    # Set plotting style
    markers,sizes,edgecolors,facecolors,labels,linewidths = set_plotting_style(obs_types)

    # Add map pins, with a legend label for each new pin category
    ax.set_title(title)
    obslabels = []
    i=0
    for lo,la,ma,si,lw,eco,fco,obs,lab in zip(alons,alats,markers,sizes,linewidths,edgecolors,facecolors,obs_types,labels):
        if obs not in obslabels:
            obslabels.append(obs)
            ax.scatter(lo,la,marker=ma,color=fco,edgecolors=eco,linewidths=lw,s=si,transform=ccrs.PlateCarree(),label=lab,zorder=1000)
        else:
            ax.scatter(lo,la,marker=ma,color=fco,edgecolors=eco,linewidths=lw,s=si,transform=ccrs.PlateCarree(),zorder=1000)
        i+=1

    if add_domains:
        ax.plot(domain_lons,domain_lats,transform=ccrs.PlateCarree(),color='k',dashes=[3,1],linewidth=0.75)

    return ax

def fix_legend_labels(h,l,custom_order=None):
    import operator
    
    unique_labels,indices = np.unique(np.array(l),return_index=True)
    unique_handles = [h[i] for i in indices]

    hl = sorted(zip(unique_handles, unique_labels), key=operator.itemgetter(1))
    hg, lg = zip(*hl)
    if custom_order is not None:
        handles_out = [hg[idx] for idx in custom_order]
        labels_out = [lg[idx] for idx in custom_order]
    else:
        handles_out = hg
        labels_out = lg

    return handles_out,labels_out

if __name__ == "__main__":

    # Read data from csv files
    sites_p,lats_p,lons_p,elev_p,obs_type_p = read_csv("TEAMx_map_info - Permanent.csv")
    sites_s,lats_s,lons_s,elev_s,obs_type_s = read_csv("TEAMx_map_info - Summer.csv")
    sites_w,lats_w,lons_w,elev_w,obs_type_w = read_csv("TEAMx_map_info - Winter.csv")

    # Convert to KML
    # write_kml("TEAMx_map_info - Permanent.csv","TEAMx_permanent.kml")

    # Define the desired map projection
    proj = ccrs.LambertConformal(central_longitude=11.4,central_latitude=46.95,standard_parallels=(45,49))

    # Plot maps
    # Note: map projection can be anything, but since the coordinate arrays are in degrees of latitude-longitude,
    # "transform=ccrs.PlateCarree()" has to be specified in each pyplot call. This works because PlateCarree is an
    # un-projected projection, that is, a geo position (lat,long) in degrees corresponds to grid values y=lat; x=long
    # on a PlateCarree map. Specifying "transform=ccrs.PlateCarree()" is equivalent to stating that the original
    # data coordinates are cast on a latitude-longitude grid.

    ############################################################################
    # VERSION 1
    ############################################################################
    fig_id = 1
    fig = p.figure(fig_id)
    fig.patch.set_facecolor('whitesmoke')

    # Set up axes (one for the map, another one for the legend)
    fig.set_size_inches(8,4)
    ax1 = p.axes([0.07, 0.06, 0.44, 0.88],projection=proj)
    ax4 = p.axes([0.57, 0.06, 0.44, 0.88])

    # Define map extent in m (0,0 is the map center)
    offset = np.array([0,0,0,0])
    extent = np.array([-0.075,0.1,-0.065,0.11])
    mapextent = offset*1e6 + extent*1e6 

    # Draw map background
    ax1 = draw_map_background(ax1,mapextent,
                              zorder_grid=500,
                              xgrid = np.arange(-30.,50.,0.5),
                              ygrid = np.arange(30.,75.,0.5))

    # Add TOC info
    # Doesn't work as it should. If a special obs site is used both in summer
    # and in winter, map pins are duplicated.
    ax1 = draw_toc_info(ax1,'TOC observations',
                       sites_s+sites_w+sites_p,
                       lons_s+lons_w+lons_p,
                       lats_s+lats_w+lats_p,
                       obs_type_s+obs_type_w+obs_type_p)

    # Draw legend
    h, l = ax1.get_legend_handles_labels()
    hh,ll = fix_legend_labels(h,l)
    ax4.axis('off')
    ax4.legend(hh,ll,frameon=False,loc='upper left')

    # Save figure
    fig.savefig("TEAMx_map_v1.png",format='png',dpi=300)
    p.close(fig)

    ############################################################################
    # VERSION 2
    ############################################################################
    fig_id = 2
    fig = p.figure(fig_id)
    fig.patch.set_facecolor('whitesmoke')

    # Set up axes (two for the maps, another one for the legend)
    fig.set_size_inches(11,4)
    ax1 = p.axes([0.03, 0.06, 0.4, 0.88],projection=proj)
    ax2 = p.axes([0.41, 0.06, 0.4, 0.88],projection=proj)
    ax4 = p.axes([0.79, 0.06, 0.12, 0.88])

    # Draw map background
    ax1 = draw_map_background(ax1,mapextent,zorder_grid=500)
    ax2 = draw_map_background(ax2,mapextent,zorder_grid=500)

    # Add summer TOC info
    ax1 = draw_toc_info(ax1,'summer TOC observations',
                       sites_s+sites_p,
                       lons_s+lons_p,
                       lats_s+lats_p,
                       obs_type_s+obs_type_p)

    # Add summer TOC info
    ax2 = draw_toc_info(ax2,'winter TOC observations',
                       sites_w+sites_p,
                       lons_w+lons_p,
                       lats_w+lats_p,
                       obs_type_w+obs_type_p)

    # Draw legend
    h1, l1 = ax1.get_legend_handles_labels()
    h2, l2 = ax2.get_legend_handles_labels()
    h = h1+h2
    l = l1+l2
    hh,ll = fix_legend_labels(h,l)
    ax4.axis('off')
    ax4.legend(hh,ll,frameon=False,loc='upper left')

    # Save figure
    fig.savefig("TEAMx_map_v2.png",format='png',dpi=300)
    p.close(fig)

    ############################################################################
    # VERSION 3
    ############################################################################
    fig_id = 3
    fig = p.figure(fig_id)
    fig.patch.set_facecolor('whitesmoke')

    # Set up axes (three for the maps, another one for the legend)
    fig.set_size_inches(8,8)
    ax1 = p.axes([0.07, 0.53, 0.4, 0.4],projection=proj)
    ax2 = p.axes([0.07, 0.03, 0.4, 0.4],projection=proj)
    ax3 = p.axes([0.57, 0.03, 0.4, 0.4],projection=proj)
    ax4 = p.axes([0.57, 0.53, 0.4, 0.4])

    # Draw map background
    ax1 = draw_map_background(ax1,mapextent,zorder_grid=500)
    ax2 = draw_map_background(ax2,mapextent,zorder_grid=500)
    ax3 = draw_map_background(ax3,mapextent,zorder_grid=500)

    # Add permanent obs
    ax1 = draw_toc_info(ax1,'permanent observations',
                       sites_p,
                       lons_p,
                       lats_p,
                       obs_type_p)

    # Add summer TOC info
    ax2 = draw_toc_info(ax2,'summer TOC observations',
                       sites_s,
                       lons_s,
                       lats_s,
                       obs_type_s)

    # Add summer TOC info
    ax3 = draw_toc_info(ax3,'winter TOC observations',
                       sites_w,
                       lons_w,
                       lats_w,
                       obs_type_w)

    # Draw legend
    h1, l1 = ax1.get_legend_handles_labels()
    h2, l2 = ax2.get_legend_handles_labels()
    h3, l3 = ax2.get_legend_handles_labels()
    h = h1+h2+h3
    l = l1+l2+l3
    hh,ll = fix_legend_labels(h,l,custom_order = [0,4,7,6,9,3,10,2,5,1,11,8])
    ax4.axis('off')
    ax4.legend(hh,ll,frameon=False,loc='upper left')

    # Save figure
    fig.savefig("TEAMx_map_v3.png",format='png',dpi=300)
    p.close(fig)